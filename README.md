# PokeTeam

###Clone this project
git clone https://gitlab.com/Valkoff/poketeam.git

##Installation
```docker
cd docker

docker-compose up
```
The first time you execute this, you need to launch:
```docker
docker-compose start migrations
```

##Live example
A working example is now live on http://valkoff.com:8080/team/list

