import $ from 'jquery';

const catchButton = $('#catch');
const deleteButton = $('.deletePokemon');
const saveTeam = $('#saveTeam');
const team = $('#team');
const filter = $('#filter');
const teams = $('.team');
const maxTeamSize = 6;


function checkTeam(updateButton = true) {
    if (getTeamSize() < maxTeamSize) {
        teamInComplete();
        if (updateButton) {
            enableCatchButton();
        }
        return false;
    } else {
        teamComplete();
        if (updateButton) {
            disableCatchButton();
        }
        return true;
    }
}

function catchPokemon() {
    disableCatchButton();
    if (!checkTeam(false)) {
        $.get('/api/team/catch', function (data) {
            team.append(data);
            deletePokemon();
        }).always(function () {
            checkTeam();
        });
    }
}

function getTeamSize() {
    return team.find('.pokemon').length;
}

function teamComplete() {
    catchButton.hide();
    saveTeam.removeClass('is-hidden');
}

function teamInComplete() {
    catchButton.show();
    saveTeam.addClass('is-hidden');
}

function enableCatchButton() {
    catchButton.click(function () {
        catchPokemon()
    });
}

function disableCatchButton() {
    catchButton.unbind('click');
}

function deletePokemon() {
    deleteButton.click(function () {
        $(this).closest('.pokemon').remove();
        checkTeam();
    });
}

function filterTeams() {
    filter.on('keyup', function () {
        let searchString = this.value;
        if (searchString && searchString.length > 2) {
            let elements = teams.has('.type-buttons .' + searchString);
            teams.hide();
            elements.show();
        } else {
            teams.show();
        }
    });
}


function initTeam() {
    checkTeam();
    deletePokemon();
    filterTeams();
}

export default initTeam;