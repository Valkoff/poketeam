<?php

namespace App\Controller\Api;

use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamApiController extends AbstractController
{
    /**
     * @Route("/api/team/catch", name="catchPokemon")
     * @param  TeamService  $service
     * @return Response
     */
    public function index(TeamService $service): Response
    {
        $pokemon = $service->getRandomPokemon($this->getDoctrine());
        return $this->render('team/pokemon.html.twig', [
            'pokemon' => $pokemon,
        ]);
    }

}
