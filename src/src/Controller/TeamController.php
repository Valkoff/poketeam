<?php

namespace App\Controller;

use App\Entity\Team;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    /**
     * @Route("/team/list", name="team")
     * @param  TeamService  $service
     * @return Response
     */
    public function index(TeamService $service): Response
    {
        $teams = $service->getAll($this->getDoctrine());
        return $this->render('team/index.html.twig', [
            'controller_name' => 'TeamController',
            'teams' => $teams,
        ]);
    }

    /**
     * @Route("/team/create", name="team_create", methods={"GET"})
     */
    public function create(): Response
    {
        return $this->render('team/form.html.twig', [
            'action' => $this->generateUrl('action_team_create'),
            'title' => 'Create a Team',
        ]);
    }

    /**
     * @Route("/team/create", name="action_team_create", methods={"POST"})
     * @param  Request  $request
     * @param  TeamService  $service
     * @return RedirectResponse
     */
    public function actionCreate(Request $request, TeamService $service): RedirectResponse
    {
        $name = $request->get('name');
        $pokemon = $request->get('pokemon');
        $service->createTeam($name, $pokemon, $this->getDoctrine());
        return $this->redirectToRoute('team');
    }

    /**
     * @Route("/team/{teamId}/edit", name="team_edit", methods={"GET"})
     * @param  int  $teamId
     * @return Response
     */
    public function edit(int $teamId): Response
    {
        $team = $this->getDoctrine()->getRepository(Team::class)->find($teamId);
        return $this->render('team/form.html.twig', [
            'action' => $this->generateUrl('action_team_edit', ['teamId' => $teamId]),
            'title' => 'Edit your Team',
            'team' => $team,
        ]);
    }

    /**
     * @Route("/team/{teamId}/edit", name="action_team_edit", methods={"POST"})
     * @param  int  $teamId
     * @param  Request  $request
     * @param  TeamService  $service
     * @return RedirectResponse
     */
    public function actionEdit(int $teamId, Request $request, TeamService $service): RedirectResponse
    {
        $team = $this->getDoctrine()->getRepository(Team::class)->find($teamId);

        $name = $request->get('name');
        $pokemon = $request->get('pokemon');

        $service->updateTeam($team, $name, $pokemon, $this->getDoctrine());
        return $this->redirectToRoute('team');
    }


}
