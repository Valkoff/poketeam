<?php

namespace App\Entity;

use App\Service\PokemonService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pokemon
 *
 * @ORM\Table(name="pokemon")
 * @ORM\Entity
 */
class Pokemon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    public $name;

    /**
     * @var int
     *
     * @ORM\Column(name="exp", type="integer", nullable=false)
     */
    public $exp;

    /**
     * @ORM\ManyToMany(targetEntity="Type")
     * @ORM\JoinTable(name="pokemon_type",
     *     joinColumns={@ORM\JoinColumn(name="pokemon_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")}
     *     )
     */
    public $types;

    /**
     * @ORM\ManyToMany(targetEntity="Ability")
     * @ORM\JoinTable(name="pokemon_ability",
     *     joinColumns={@ORM\JoinColumn(name="pokemon_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="ability_id", referencedColumnName="id")}
     *     )
     */
    public $abilities;

    /**
     * @var string
     */
    private $sprite;

    /**
     * Pokemon constructor.
     * @param  int  $id
     * @param  string  $name
     * @param  int  $exp
     * @param  array  $types
     * @param  array  $abilities
     */
    public function __construct($id, $name, $exp, $types, $abilities)
    {
        $this->id = $id;
        $this->name = $name;
        $this->exp = $exp;
        $this->types = new ArrayCollection();
        $this->abilities = new ArrayCollection();

        foreach ($types as $type) {
            $this->addType($type);
        }
        foreach ($abilities as $ability) {
            $this->addAbility($ability);
        }
    }

    /**
     * @param  Type  $type
     * @return Pokemon
     */
    public function addType(Type $type): Pokemon
    {
        if ( ! $this->types->contains($type)) {
            $this->types[] = $type;
        }
        return $this;
    }

    /**
     * @param  Ability  $ability
     * @return Pokemon
     */
    public function addAbility(Ability $ability): Pokemon
    {
        if ( ! $this->abilities->contains($ability)) {
            $this->abilities[] = $ability;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getSprite(): string
    {
        return '/sprites/'.$this->name.'.png';
    }

    /**
     * @param  string  $sprite
     */
    public function setSprite(string $sprite): void
    {
        $service = new PokemonService();
        $service->setSprite($this->name, $sprite);
        $this->sprite = $this->getSprite();
    }


}
