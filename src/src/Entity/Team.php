<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @ORM\ManyToMany(targetEntity="Pokemon")
     * @ORM\JoinTable(name="team_pokemon",
     *     joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="pokemon_id", referencedColumnName="id")}
     *     )
     */
    private $pokemon;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;
    /**
     * @var DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->pokemon = new ArrayCollection();
        $this->created = new DateTime();
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param  string  $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPokemon()
    {
        return $this->pokemon;
    }

    /**
     * @param  Pokemon  $pokemon
     * @return Team
     */
    public function addPokemon(Pokemon $pokemon)
    {
        if ( ! $this->pokemon->contains($pokemon)) {
            $this->pokemon[] = $pokemon;
        }
        return $this;
    }

    /**
     * @return Team
     */
    public function removeAllPokemon()
    {
        $this->pokemon->clear();
        return $this;
    }

    public function getPokemonTypes(): array
    {
        $types = [];
        foreach ($this->pokemon as $pokemon) {
            foreach ($pokemon->types as $type) {
                $types[$type->getLabel()] = $type;
            }
        }
        return $types;
    }

    public function getTotalExp()
    {
        $exp = 0;
        foreach ($this->pokemon as $pokemon) {
            $exp += $pokemon->exp;
        }
        return $exp;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
