<?php

namespace App\Service;


use App\Entity\Ability;
use Doctrine\Persistence\ManagerRegistry;

class AbilityService
{

    /**
     * @param $label
     * @param  ManagerRegistry  $doctrine
     * @return Ability
     */
    public function getByLabel($label, ManagerRegistry $doctrine): Ability
    {
        $entity = $doctrine->getRepository(Ability::class)->findOneByLabel($label);
        if ( ! $entity instanceof Ability) {
            return $this->create($label, $doctrine);
        }
        return $entity;
    }

    /**
     * @param $ability
     * @param  ManagerRegistry  $doctrine
     * @return Ability
     */
    public function create($ability, ManagerRegistry $doctrine): Ability
    {
        $entity = new Ability($ability);
        $doctrine->getManager()->persist($entity);
        $doctrine->getManager()->flush();
        return $entity;
    }

}