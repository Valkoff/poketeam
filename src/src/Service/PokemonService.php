<?php

namespace App\Service;


use App\Entity\Pokemon;
use Doctrine\Persistence\ManagerRegistry;
use PokePHP\PokeApi;
use Symfony\Component\Config\Definition\Exception\Exception;

class PokemonService
{


    public const KNOWN_SPECIES = 893;

    /**
     * @param  ManagerRegistry  $doctrine
     * @return Pokemon
     * @throws \Exception
     */
    public function getRandomPokemon(ManagerRegistry $doctrine): Pokemon
    {
        $randomId = random_int(1, self::KNOWN_SPECIES);
        $entity = $this->getById($randomId, $doctrine);
        if ( ! $entity instanceof Pokemon) {
            $pokemon = $this->getPokemonViaApi($randomId);
            return $this->create($pokemon, $doctrine);
        }
        return $entity;
    }

    /**
     * @param  int  $pokemonId
     * @param  ManagerRegistry  $doctrine
     * @return Pokemon|object|null
     */
    public function getById(int $pokemonId, ManagerRegistry $doctrine)
    {
        return $doctrine->getRepository(Pokemon::class)->find($pokemonId);
    }

    /**
     * @param $id
     * @return object|string
     */
    public function getPokemonViaApi($id)
    {
        try {
            $api = new PokeApi();
            do {
                $pokemon = json_decode($api->pokemon($id), false);
            } while ($pokemon === 'An error has occured.');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
        return $pokemon;
    }

    /**
     * @param  object  $pokemon
     * @param  ManagerRegistry  $doctrine
     * @return Pokemon
     */
    public function create(object $pokemon, ManagerRegistry $doctrine): Pokemon
    {

        $typeService = new TypeService();
        $abilityService = new AbilityService();
        $types = [];
        $abilities = [];

        foreach ($pokemon->types as $type) {
            $types[] = $typeService->getByLabel($type->type->name, $doctrine);
        }

        foreach ($pokemon->abilities as $ability) {
            $abilities[] = $abilityService->getByLabel($ability->ability->name, $doctrine);
        }

        $entity = new Pokemon($pokemon->id, $pokemon->name, $pokemon->base_experience, $types, $abilities);
        $entity->setSprite($pokemon->sprites->front_default);
        $doctrine->getManager()->persist($entity);
        $doctrine->getManager()->flush();
        return $entity;
    }

    /**
     * @param $pokemonName
     * @param  string  $url
     */
    public function setSprite($pokemonName, $url): void
    {
        $contents = file_get_contents($url);
        $file = @fopen('sprites/'.$pokemonName.'.png', 'w');
        if ($file !== false) {
            fwrite($file, $contents);
            fclose($file);
        }
    }

}