<?php

namespace App\Service;


use App\Entity\Pokemon;
use App\Entity\Team;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Config\Definition\Exception\Exception;

class TeamService
{


    /**
     * @param  ManagerRegistry  $doctrine
     * @return array
     */
    public function getAll(ManagerRegistry $doctrine): array
    {
        return $doctrine->getRepository(Team::class)->findAll();
    }

    /**
     * @param  ManagerRegistry  $doctrine
     * @return Pokemon|string
     */
    public function getRandomPokemon(ManagerRegistry $doctrine)
    {
        $pokemonService = new PokemonService();
        try {
            return $pokemonService->getRandomPokemon($doctrine);
        } catch (Exception $exception) {
            return $exception->getMessage();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param  string  $name
     * @param  array  $pokemons
     * @param  ManagerRegistry  $doctrine
     * @return bool
     */
    public function createTeam($name, array $pokemons, ManagerRegistry $doctrine): bool
    {
        $team = new Team();
        $team->setName($name);
        foreach ($pokemons as $teamMember) {
            $entity = $doctrine->getRepository(Pokemon::class)->find($teamMember);
            $team->addPokemon($entity);
        }
        $doctrine->getManager()->persist($team);
        $doctrine->getManager()->flush();
        return true;
    }

    /**
     * @param  Team  $team
     * @param $name
     * @param  array  $pokemons
     * @param  ManagerRegistry  $doctrine
     * @return bool
     */
    public function updateTeam(Team $team, $name, array $pokemons, ManagerRegistry $doctrine): bool
    {
        $team->setName($name);
        $team->removeAllPokemon();
        foreach ($pokemons as $teamMember) {
            $entity = $doctrine->getRepository(Pokemon::class)->find($teamMember);
            $team->addPokemon($entity);
        }
        $doctrine->getManager()->persist($team);
        $doctrine->getManager()->flush();
        return true;
    }

}