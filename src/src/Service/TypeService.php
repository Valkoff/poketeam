<?php

namespace App\Service;


use App\Entity\Type;
use Doctrine\Persistence\ManagerRegistry;

class TypeService
{

    /**
     * @param $label
     * @param  ManagerRegistry  $doctrine
     * @return Type
     */
    public function getByLabel($label, ManagerRegistry $doctrine): Type
    {
        $entity = $doctrine->getRepository(Type::class)->findOneByLabel($label);
        if ( ! $entity instanceof Type) {
            return $this->create($label, $doctrine);
        }
        return $entity;
    }

    /**
     * @param $type
     * @param  ManagerRegistry  $doctrine
     * @return Type
     */
    public function create($type, ManagerRegistry $doctrine): Type
    {
        $entity = new Type($type);
        $doctrine->getManager()->persist($entity);
        $doctrine->getManager()->flush();
        return $entity;
    }

}